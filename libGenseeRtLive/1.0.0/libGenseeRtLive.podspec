#
#  Be sure to run `pod spec lint libGenseeRt+VodSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

 
  s.name         = "libGenseeRtLive"
  s.version      = "1.0.0"
  s.summary      = "展示互动 RtSDK 特殊处理的动态库"

  s.description  = <<-DESC
                      为了方便接入展示互动的SDK 
                   DESC

  s.homepage     = "http://www.gensee.com"

  s.license      = "MIT"

  s.author             = { "caolixiao" => "caolixiao@yeah.net" }
  
  s.platform     = :ios, "8.0"

  s.ios.deployment_target = "8.0"

  s.source       = { :git => "https://caolixiao@bitbucket.org/caolixiao/libgenseesdk.git", :tag => "v#{s.version}" }


  s.resources = "RtSDK/RtSDK.bundle"
  s.resources = "RtSDK/RtSDK.bundle", "RtSDK/Assets/*.png"
  
  s.ios.preserve_paths       = 'RtSDK/RtSDK.framework'
  s.ios.source_files         = 'RtSDK/RtSDK.framework/Headers/*.h'
  s.ios.public_header_files  = 'RtSDK/RtSDK.framework/Headers/*.h'
  s.ios.vendored_frameworks  = 'RtSDK/RtSDK.framework'

  s.frameworks = "OpenAL", "GLKit", "CoreVideo", "CoreMedia", "VideoToolbox", "SystemConfiguration"
  s.libraries = "iconv", "sqlite3.0", "z", "c++"

  s.requires_arc = true
  # s.xcconfig = {"ENABLE_BITCODE" => "NO", "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES"}

end
