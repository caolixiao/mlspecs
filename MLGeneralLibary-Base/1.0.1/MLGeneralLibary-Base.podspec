
Pod::Spec.new do |s|

  s.name         = "MLGeneralLibary-Base"
  s.version      = "1.0.1"
  s.summary      = "A short description of MLGeneralLibary."
  s.description  = "基础的功能"

  s.homepage     = "https://caolixiao@bitbucket.org/caolixiao/mlgenerallibary"
  s.license      = "MIT"
  s.author       = { "caolixiao" => "caolixiao@yeah.net" }
  s.platform     = :ios, "8.0"
  s.ios.deployment_target = "8.0"
  s.source       = { :git => "https://caolixiao@bitbucket.org/caolixiao/mlgenerallibary.git", :tag => "#{s.version}" }


  s.source_files        = "MLConfig/*.{h,m,mm}", "MLCategorys/MLObjc/**/*.{h,m,mm}", "MLCategorys/MLChange/**/*.{h,m,mm}", "MLCategorys/MLImgDownload/**/*.{h,m,mm}", "MLCategorys/MLNav/**/*.{h,m,mm}", "MLCategorys/MLView/**/*.{h,m,mm}", "MLViewController/MLBaseViewController/**/*.{h,m,mm}", "MLCustomViews/MLNavViewBar/*.{h,m,mm}", "MLCustomViews/MLPageView/*.{h,m,mm}", "MLCustomViews/MLPageView/**/*.{h,m,mm}"
  s.public_header_files = "MLConfig/*.h", "MLCategorys/MLObjc/**/*.h", "MLCategorys/MLChange/**/*.h", "MLCategorys/MLImgDownload/**/*.h", "MLCategorys/MLNav/**/*.h", "MLCategorys/MLView/**/*.h", "MLViewController/MLBaseViewController/**/*.h", "MLCustomViews/MLNavViewBar/*.h", "MLCustomViews/MLPageView/*.h", "MLCustomViews/MLPageView/**/*.h"
 
  s.requires_arc = true
  s.prefix_header_contents = '#import "MLGlobalHeader.h"'

  s.dependency "DZNEmptyDataSet"
  s.dependency "MJRefresh"

end
