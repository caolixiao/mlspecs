
Pod::Spec.new do |s|

  s.name         = "UTDID"
  s.version      = "0.0.3"
  s.summary      = "A short description of UTDID."

  s.description  = " == "

  s.homepage     = "https://github.com/easefun/polyv-ios-sdk/wiki"

  s.license      = "MIT"

  s.author       = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:caolixiao/libpolyvsdk.git", :tag => "#{s.version}" }

  s.vendored_frameworks = "libpolyvSDK/AdditionalLibrary/UTDID.framework"

  s.requires_arc = true

end
