#
# Be sure to run `pod lib lint libGoldenRudder.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'libGoldenRudder'
  s.version          = '1.2.1'
  s.summary          = 'A short description of libGoldenRudder.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.homepage         = 'https://bitbucket.org/caolixiao/goldenrudderdemo'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'toby' => 'caolixiao@yeah.net' }
  s.source           = { :git => 'https://caolixiao@bitbucket.org/caolixiao/goldenrudderdemo.git', :tag => "#{s.version}" }

  s.ios.deployment_target = '8.0'

  s.vendored_frameworks = 'GoldenRudderFramework-IOS/GoldenRudderFramework.framework'
  # s.public_header_files = 'GoldenRudderFramework-IOS/GoldenRudderFramework.framework/Headers/*.h'

end
