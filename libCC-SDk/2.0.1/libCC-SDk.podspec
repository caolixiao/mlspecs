
Pod::Spec.new do |s|

  s.name         = "libCC-SDk"
  s.version      = "2.0.1"
  s.summary      = "CC视屏 -> 3.1.8 版本"
  s.description  = <<-DESC
                        CC视屏点播SDK接入
                   DESC

  s.homepage     = "http://www.bokecc.com"
  s.license      = "MIT"
  s.author             = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "8.0"
  s.ios.deployment_target = "8.0"
  s.source       = { :git => "git@bitbucket.org:caolixiao/libcc-sdk.git", :tag => "v#{s.version}" }

  s.source_files = "3.1.8/include/*.h"
  s.public_header_files = "3.1.8/include/*.h"

 
  s.frameworks = "AVFoundation", "CoreLocation", "CoreMedia", "CoreGraphics"
  s.vendored_library = "3.1.8/include/libCCSDK.a"

  s.requires_arc = true

end
