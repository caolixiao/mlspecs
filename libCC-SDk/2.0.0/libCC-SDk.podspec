
Pod::Spec.new do |s|

  s.name         = "libCC-SDk"
  s.version      = "2.0.0"
  s.summary      = "CC视屏 -> 3.1.8 版本"
  s.description  = <<-DESC
                        CC视屏点播SDK接入
                   DESC

  s.homepage     = "http://www.bokecc.com"
  s.license      = "MIT"
  s.author             = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "9.0"
  s.ios.deployment_target = "9.0"
  s.source       = { :git => "git@gitlab.yunduoketang.com.cn:app/libCCSdk.git", :tag => "v#{s.version}" }

  s.source_files = "3.1.8/include/*.h"
  s.public_header_files = "3.1.8/include/*.h"

 
  s.frameworks = "AVFoundation", "CoreLocation", "CoreMedia", "CoreGraphics"
  s.vendored_library = "3.1.8/include/libCCSDK.a"

  s.requires_arc = true

end
