
Pod::Spec.new do |s|

  s.name         = "AlipaySDK-No-UTDID"
  s.version      = "0.0.8"
  s.summary      = "AlipaySDK-No-UTDID -> v15.4.1  url -> https://docs.open.alipay.com/204/105295/"

  s.homepage     = "https://bitbucket.org/caolixiao/libalipaysdk"
  
  s.license      = "MIT"
  
  s.author             = { "caolixiao" => "caolixiao@yeah.net" }
  
  s.platform     = :ios, "8.0"
  s.ios.deployment_target = "8.0"

  s.source       = { :git => "https://caolixiao@bitbucket.org/caolixiao/libalipaysdk.git", :tag => "#{s.version}" }

  s.resource = "AlipaySDK_No_UTDID/IOS/AlipaySDK.bundle"
  
  s.vendored_frameworks = "AlipaySDK_No_UTDID/IOS/AlipaySDK.framework"
  
  s.frameworks = "SystemConfiguration", "CoreTelephony", "QuartzCore", "CoreText", "CoreGraphics", "CFNetwork", "CoreMotion"
  s.libraries = "c++", "z"

  s.requires_arc = true

end
