Pod::Spec.new do |s|
  
  s.name         = "LibBaseSDK"
  s.version      = "1.0.1"
  s.summary      = "LibBaseSDK SDK."
  s.description  = "LibBaseSDK SDK for iOS."
  
  s.homepage     = "http://www.toby.ml"
  s.license      = "MIT"
  s.author       = { "caolixiao" => "caolixiao@yeah.net" }
  
  s.platform     = :ios, "9.0"
  s.ios.deployment_target = "9.0"
  
  s.source       = {
    :git => "https://caolixiao@bitbucket.org/caolixiao/libbasesdk.git",
    :tag => "v#{s.version}"
  }
  
  s.requires_arc = true
  s.static_framework = true
  s.swift_version = '5.0'
  s.prefix_header_contents = <<-DESC
  
  DESC
  
  s.pod_target_xcconfig = {
    "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) LibBaseSDK_NAME=#{s.name} LibBaseSDK_VERSION=#{s.version}"
  }
  
  s.default_subspecs = ["Base", "BaseSDK", "Config", "Color", "SysClassExt", "Sturdy", "AppDelegate", "Network", "Caches", ]
  
  s.subspec "Base" do |ss|
#    ss.source_files = ["Foundation/**/*.{h,m}", "UIKit/**/*.{h,m}", "Module/module.modulemap"]
    ss.source_files = ["Foundation/**/*.{h,m}", "UIKit/**/*.{h,m}"]
    ss.public_header_files = [ "Foundation/**/*.h", "UIKit/**/*.h"]
    ss.frameworks = ["Foundation", "UIKit"]
    ss.libraries = ['icucore', 'c++']
#    ss.preserve_path = "Module/module.modulemap"
#
#    ss.user_target_xcconfig = {
#      "SWIFT_INCLUDE_PATHS" => "$(PODS_ROOT)/Module"
#    }
#
#    ss.pod_target_xcconfig = {
#      "SWIFT_INCLUDE_PATHS" => "$(PODS_ROOT)/Module"
#    }
  end
  
  s.subspec "BaseSDK" do |ss|
    ss.source_files =
    "BaseSDK/*.swift"
    
    ss.frameworks = "AdSupport"
    ss.libraries = "iconv", "z", "sqlite3"
  end
  
  ### Config
  s.subspec "Config" do |ss|
    ss.source_files  = "Config/*.swift"
  end
  
  ### Color
  s.subspec "Color" do |ss|
    ss.source_files  = "Color/*.swift"
  end
  
  ### SysClassExt
  s.subspec "SysClassExt" do |ss|
    ss.public_header_files =
    "SysClassExt/*.h",
    "SysClassExt/**/*.h"

    ss.source_files =
    "SysClassExt/*.swift",
    "SysClassExt/*.{h,m}",
    "SysClassExt/**/*.{h,m}"
  end

  ### Sturdy
  s.subspec "Sturdy" do |ss|
    ss.source_files = "Sturdy/*.swift"
  end
  
  ### AppDelegate
  s.subspec "AppDelegate" do |ss|
    ss.source_files = "AppDelegate/*.swift"
  end
  
  ### Network
  s.subspec "Network" do |ss|
    ss.public_header_files =
    "Network/HttpSession/*.h"
    
    ss.source_files =
    "Network/BaseNet/*.swift",
    "Network/BaseSocket/*.swift",
    "Network/HttpSession/*.{h,m,swift}",
    "Network/ReachabilityExt/*.swift"

    ### 网络 解析
    ss.dependency 'HandyJSON'
    ss.dependency 'Alamofire'
    ss.dependency 'AlamofireImage'
    
    ### socket
    ss.dependency 'Socket.IO-Client-Swift'#, '~> 15.2'
    ss.dependency 'LibBaseSDK/BaseSDK'
  end
  
  ### Caches
  s.subspec "Caches" do |ss|
    ss.source_files = "Caches/*.swift"
  end

end
