
Pod::Spec.new do |s|

  s.name         = "libPolyvSDK"
  s.version      = "3.0.0"
  s.summary      = "A short description of libPolyvSDKk."

  s.description  = " == "

  s.homepage     = "https://github.com/easefun/polyv-ios-sdk/wiki"

  s.license      = "MIT"

  s.author       = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:caolixiao/libpolyvsdk.git", :tag => "v#{s.version}" }

  s.source_files  = "libpolyvSDK20180102/libpolyvSDK/*.h"
  s.public_header_files = "libpolyvSDK20180102/libpolyvSDK/*.h"

  s.vendored_library = "libpolyvSDK20180102/libpolyvSDK.a"
  s.vendored_frameworks = "libpolyvSDK20180102/AdditionalLibrary/AlicloudHttpDNS.framework", "libpolyvSDK20180102/AdditionalLibrary/AlicloudUtils.framework", "libpolyvSDK20180102/AdditionalLibrary/UTDID.framework"

  s.requires_arc = true

  s.xcconfig = {  'OTHER_LDFLAGS' => '-lresolv -lz -lsqlite3.0 -licucore -framework MobileCoreServices -framework SystemConfiguration -framework CoreTelephony'} 

end
