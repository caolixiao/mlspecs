
Pod::Spec.new do |s|

  s.name         = "libPolyvSDK"
  s.version      = "0.0.3"
  s.summary      = "A short description of libPolyvSDK."

  s.description  = " == "

  s.homepage     = "https://github.com/easefun/polyv-ios-sdk/wiki"

  s.license      = "MIT"

  s.author       = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:caolixiao/libpolyvsdk.git", :tag => "#{s.version}" }

  s.source_files  = "libpolyvSDK/libpolyvSDK/*.h", "libpolyvSDK/libpolyvSDK/PLVControl/*.{h,m}"
  s.public_header_files = "libpolyvSDK/libpolyvSDK/*.h", "libpolyvSDK/libpolyvSDK/PLVControl/*.h"
  
  s.vendored_library = "libpolyvSDK/libpolyvSDK.a"
  
  s.frameworks = "MobileCoreServices", "SystemConfiguration", "CoreTelephony"
  s.libraries = "resolv", "z", "sqlite3.0", "icucore"

  # s.dependency 'libPolyvDepSDK', '~> 0.0.3'

  s.requires_arc = true

end
