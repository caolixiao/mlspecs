
Pod::Spec.new do |s|

  s.name         = "libPolyvSDK"
  s.version      = "2.0.0"
  s.summary      = "A short description of libPolyvSDK."

  s.description  = " == "

  s.homepage     = "https://github.com/easefun/polyv-ios-sdk/wiki"

  s.license      = "MIT"

  s.author       = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "9.0"

  s.source       = { :git => "https://caolixiao@bitbucket.org/caolixiao/libpolyvsdk.git", :tag => "v#{s.version}" }

  s.vendored_frameworks = "PLVVodSDK.framework"

  s.dependency 'PLVMarquee', '0.0.2'
  s.dependency "PLVNetworkDiagnosisTool", '0.0.1'
  s.dependency "PLVTimer", '0.0.3'
  s.dependency 'PolyvAliHttpDNS', '1.6.7'
  s.dependency "PolyvIJKPlayer_Dylib", '0.5.0'
  s.dependency 'SQLiteRepairKit', '1.2.1'
  s.dependency "WCDB", '1.0.7.5'
  s.dependency "WCDBOptimizedSQLCipher", '1.2.1'

  s.requires_arc = true
end
