
Pod::Spec.new do |s|

  s.name         = "libPolyvDepSDK"
  s.version      = "0.0.3"
  s.summary      = "A short description of libPolyvDepSDK."

  s.description  = " == "

  s.homepage     = "https://github.com/easefun/polyv-ios-sdk/wiki"

  s.license      = "MIT"

  s.author       = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:caolixiao/libpolyvsdk.git", :tag => "#{s.version}" }
  
  s.vendored_library = "libpolyvSDK/libpolyvSDK.a"
  s.vendored_frameworks = "libpolyvSDK/AdditionalLibrary/AlicloudHttpDNS.framework", "libpolyvSDK/AdditionalLibrary/AlicloudUtils.framework", "libpolyvSDK/AdditionalLibrary/UTDID.framework"

  s.frameworks = "MobileCoreServices", "SystemConfiguration", "CoreTelephony"
  s.libraries = "resolv", "z", "sqlite3.0", "icucore"

  s.requires_arc = true

end
