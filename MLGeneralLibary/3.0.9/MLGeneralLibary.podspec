
Pod::Spec.new do |s|
    
    s.name         = "MLGeneralLibary"
    s.version      = "3.0.9"
    s.summary      = "MLGeneralLibary SDK."
    s.description  = "MLGeneralLibary SDK for iOS."
    
    s.homepage     = "https://caolixiao@bitbucket.org/caolixiao/mlgenerallibary"
    s.license      = "MIT"
    s.author       = { "caolixiao" => "caolixiao@yeah.net" }
    
    s.platform     = :ios, "9.0"
    s.ios.deployment_target = "9.0"
    
    s.source       = {
        :git => "https://caolixiao@bitbucket.org/caolixiao/mlgenerallibary.git",
        :tag => "v#{s.version}"
    }
    
    s.requires_arc = true
    s.prefix_header_contents = <<-DESC
        #import "MLGlobalHeader.h"
    DESC
    
#    s.xcconfig = { "GCC_PREFIX_HEADER" => "~/MLGeneralLibary/MLConfig/MLInfo-Prefix.pch" }
    s.pod_target_xcconfig = {
        "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) MLGeneralLibary_NAME=#{s.name} MLGeneralLibary_VERSION=#{s.version}"
    }
    
    s.default_subspecs = ["Base", "Config"]
    
    s.subspec "Base" do |ss|
#        ss.ios.preserve_paths       = 'frameworks/MLGeneralLibary.framework'
#        ss.ios.source_files         = 'frameworks/MLGeneralLibary.framework/Versions/A/Headers/**/*.h'
#        ss.ios.public_header_files  = 'frameworks/MLGeneralLibary.framework/Versions/A/Headers/**/*.h'
#        # ss.ios.resource             = 'frameworks/MLGeneralLibary.framework/Versions/A/Resources/**/*'

        ss.source_files = ["Foundation/**/*.{h,m}", "UIKit/**/*.{h,m}"]
        ss.public_header_files = [ "Foundation/**/*.h", "UIKit/**/*.h"]
        ss.frameworks = ["Foundation", "UIKit"]
        
        ss.dependency "MLGeneralLibary/Categorys/MLObjc"
        ss.dependency "MLGeneralLibary/Categorys/MLChange"
        ss.dependency "MLGeneralLibary/Categorys/MLColor"
        ss.dependency "MLGeneralLibary/Categorys/MLNav"
        ss.dependency "MLGeneralLibary/BaseVC"
    end
    
    ### Config
    s.subspec "Config" do |ss|
        ss.source_files  = "MLConfig/*.{h,m,mm}"
        ss.public_header_files = "MLConfig/*.h"
        ss.frameworks = ["Foundation"]
    end
    
    ### Categorys
    s.subspec "Categorys" do |ss|
        ss.subspec "MLObjc" do |sss|
            sss.public_header_files = "MLCategorys/MLObjc/**/*.h"
            sss.source_files = "MLCategorys/MLObjc/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLChange" do |sss|
            sss.public_header_files = "MLCategorys/MLChange/**/*.h"
            sss.source_files = "MLCategorys/MLChange/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLColor" do |sss|
            sss.public_header_files = "MLCategorys/MLColor/**/*.h"
            sss.source_files = "MLCategorys/MLColor/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLNav" do |sss|
            sss.public_header_files = "MLCategorys/MLNav/**/*.h"
            sss.source_files = "MLCategorys/MLNav/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit", "QuartzCore"]
        end
        ss.subspec "MLFile" do |sss|
            sss.public_header_files = "MLCategorys/MLFile/**/*.h"
            sss.source_files = "MLCategorys/MLFile/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLImgDownload" do |sss|
            sss.public_header_files = "MLCategorys/MLImgDownload/**/*.h"
            sss.source_files = "MLCategorys/MLImgDownload/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLBundle" do |sss|
            sss.public_header_files = "MLCategorys/MLBundle/**/*.h"
            sss.source_files = "MLCategorys/MLBundle/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLText" do |sss|
            sss.public_header_files = "MLCategorys/MLText/**/*.h"
            sss.source_files = "MLCategorys/MLText/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLView" do |sss|
            sss.public_header_files =  "MLCategorys/MLView/**/*.h"
            sss.source_files = "MLCategorys/MLView/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
    end
    
    ### CustomViews
    s.subspec "CustomViews" do |ss|
        ss.subspec "MLAgreeView" do |sss|
            sss.public_header_files = "MLCustomViews/MLAgreeView/*.h"
            sss.source_files = "MLCustomViews/MLAgreeView/*.{h,m,mm}"
            sss.resource_bundles = {
                'MLAgreeView' => ['MLCustomViews/MLAgreeView/MLResources/*.png']
            }
            sss.frameworks = ["Foundation", "UIKit"]
            sss.dependency "MLGeneralLibary/Categorys/MLBundle"
        end
        ss.subspec "MLButton" do |sss|
            sss.public_header_files = "MLCustomViews/MLButton/*.h"
            sss.source_files = "MLCustomViews/MLButton/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLChoiceMenuView" do |sss|
            sss.public_header_files = "MLCustomViews/MLChoiceMenuView/*.h"
            sss.source_files = "MLCustomViews/MLChoiceMenuView/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLCircularProgressView" do |sss|
            sss.public_header_files = "MLCustomViews/MLCircularProgressView/*.h"
            sss.source_files = "MLCustomViews/MLCircularProgressView/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLCVWaterfallFlowLayout" do |sss|
            sss.public_header_files = "MLCustomViews/MLCVWaterfallFlowLayout/*.h"
            sss.source_files = "MLCustomViews/MLCVWaterfallFlowLayout/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLDrawBoard" do |sss|
            sss.public_header_files = "MLCustomViews/MLDrawBoard/*.h"
            sss.source_files = "MLCustomViews/MLDrawBoard/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
            
            sss.dependency "MLGeneralLibary/Categorys/MLFile"
            sss.dependency "MLGeneralLibary/CustomViews/MLMessageHUD"
        end
        ss.subspec "MLEditImgView" do |sss|
            sss.public_header_files = "MLCustomViews/MLEditImgView/*.h"
            sss.source_files = "MLCustomViews/MLEditImgView/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLMessageHUD" do |sss|
            sss.public_header_files = "MLCustomViews/MLMessageHUD/*.h"
            sss.source_files = "MLCustomViews/MLMessageHUD/*.{h,m,mm}"
            sss.resource_bundles = {
                'MLMessageHUD' => ['MLCustomViews/MLMessageHUD/MLResources/*.png']
            }
            sss.frameworks = ["Foundation", "UIKit"]
            sss.dependency 'FFToast', '~> 1.2.0'
            sss.dependency 'MBProgressHUD'
        end
        ss.subspec "MLNavViewBar" do |sss|
            sss.public_header_files = "MLCustomViews/MLNavViewBar/*.h"
            sss.source_files = "MLCustomViews/MLNavViewBar/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLPageView" do |sss|
            sss.public_header_files = "MLCustomViews/MLPageView/*.h", "MLCustomViews/MLPageView/**/*.h"
            sss.source_files = "MLCustomViews/MLPageView/*.{h,m,mm}", "MLCustomViews/MLPageView/**/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLRunsLanternView" do |sss|
            sss.public_header_files = "MLCustomViews/MLRunsLanternView/*.h"
            sss.source_files = "MLCustomViews/MLRunsLanternView/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLSwitch" do |sss|
            sss.public_header_files = "MLCustomViews/MLSwitch/*.h"
            sss.source_files = "MLCustomViews/MLSwitch/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLStepper" do |sss|
            sss.public_header_files = "MLCustomViews/MLStepper/*.h"
            sss.source_files = "MLCustomViews/MLStepper/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLCyclePageView" do |sss|
            sss.public_header_files = "MLCyclePageView/*.h"
            sss.source_files  = "MLCustomViews/MLCyclePageView/*.{h,m}"
            sss.resource  = "MLCustomViews/MLCyclePageView/Images.xcassets"
            sss.frameworks = ["Foundation", "UIKit"]
        end
    end
    
    ### Tools
    s.subspec "Tools" do |ss|
        ss.subspec "MLDeviceInfo" do |sss|
            sss.public_header_files = "MLTools/MLDeviceInfo/*.h"
            sss.source_files = "MLTools/MLDeviceInfo/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit", "CoreLocation", "SystemConfiguration", "NetworkExtension"]
            sss.libraries = ['icucore', 'c++']
        end
        ss.subspec "MLKeychain" do |sss|
            sss.public_header_files = "MLTools/MLKeychain/*.h"
            sss.source_files = "MLTools/MLKeychain/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit"]
        end
        ss.subspec "MLPaymentAppStore" do |sss|
            sss.public_header_files = "MLTools/MLPaymentAppStore/*.h"
            sss.source_files = "MLTools/MLPaymentAppStore/*.{h,m,mm}"
            sss.frameworks = ["Foundation", "UIKit", "StoreKit"]
            sss.dependency "MLGeneralLibary/CustomViews/MLMessageHUD"
        end
        ss.subspec "MLSwift" do |sss|
            sss.public_header_files = "MLTools/MLSwift/*.h"
            sss.source_files = "MLTools/MLSwift/*.swift"
            sss.frameworks = ["Foundation", "UIKit"]
        end
    end

     ### Net
    s.subspec "Network" do |ss|
        ss.public_header_files = "MLNetwork/*.h", "MLNetwork/**/*.h"
        ss.source_files  = "MLNetwork/*.{h,m,mm}", "MLNetwork/**/*.{h,m,mm}"
        ss.frameworks = ["Foundation", "UIKit"]
    end

    
    ### BaseVC
    s.subspec "BaseVC" do |ss|
        ss.public_header_files = "MLViewController/MLBaseViewController/**/*.h"
        ss.source_files  = "MLViewController/MLBaseViewController/**/*.{h,m,mm}"
        ss.frameworks = ["Foundation", "UIKit"]
        
        ss.dependency "DZNEmptyDataSet"
        ss.dependency "MJRefresh"
        ss.dependency "MLGeneralLibary/CustomViews/MLNavViewBar"
    end
    
    ### Version Upload
    s.subspec "VersionUpload" do |ss|
        ss.source_files  = "MLVersionUpload/*.{h,m}"
        ss.public_header_files = "MLVersionUpload/*.h"
        ss.frameworks = ["Foundation", "UIKit"]
    end
    
    ### Web
    s.subspec "Web" do |ss|
        ss.source_files  = "MLViewController/MLWebViewController/**/*.{h,m,mm}", "MLViewController/MLWebViewController/*.{h,m,mm}"
        ss.public_header_files = "MLViewController/MLWebViewController/**/*.h", "MLViewController/MLWebViewController/*.h"
        ss.resources = ["MLViewController/MLWebViewController/MLWebResources/*.png"]
        ss.resource_bundles = {
            'MLWebView' => ['MLViewController/MLWebViewController/MLWebResources/*.png']
        }
        
        ss.frameworks = ["Foundation", "UIKit", "WebKit"]
    end
    
    ### QR
    s.subspec "QR" do |ss|
        ss.public_header_files  = "MLViewController/MLQRViewController/*.h"
        ss.source_files  = "MLViewController/MLQRViewController/*.{h,m,mm}"
        ss.resources = ['MLViewController/MLQRViewController/MLQRResources/*.png']
        ss.resource_bundles = {
            'MLQRScan' => ['MLViewController/MLQRViewController/MLQRResources/*.png']
        }
        ss.frameworks = ["Foundation", "UIKit", "AVFoundation"]
    end
    
    ### ImagePicker
    s.subspec "ImagePicker" do |ss|
        ss.public_header_files  = "MLViewController/MLImagePickerController/*.h"
        ss.source_files  = "MLViewController/MLImagePickerController/*.{h,m,mm}"
        ss.frameworks = ["Foundation", "UIKit"]
    end
    
    ### Split
    s.subspec "Split" do |ss|
        ss.public_header_files  = "MLViewController/MLSplitViewController/*.h"
        ss.source_files  = "MLViewController/MLSplitViewController/*.{h,m,mm}"
        ss.frameworks = ["Foundation", "UIKit"]
    end
    
    ### User
    s.subspec "User" do |ss|
        ss.public_header_files  = "MLViewController/MLUserViewController/*.h"
        ss.source_files  = "MLViewController/MLUserViewController/*.{h,m,mm}"
        ss.frameworks = ["Foundation", "UIKit"]
    end

    ### deprecated
    s.subspec "All-DEPRECATED" do |ss|
        ss.dependency "MLGeneralLibary/Config"
    end
    
end
