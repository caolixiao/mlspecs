
Pod::Spec.new do |s|

  s.name         = "libPolyvSDKK"
  s.version      = "0.0.8"
  s.summary      = "A short description of libPolyvSDKk."

  s.description  = " == "

  s.homepage     = "https://github.com/easefun/polyv-ios-sdk/wiki"

  s.license      = "MIT"

  s.author       = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:caolixiao/libpolyvsdk.git", :tag => "#{s.version}" }

  s.source_files  = "libpolyvSDK/libpolyvSDK/*.h"
  s.public_header_files = "libpolyvSDK/libpolyvSDK/*.h"

  s.vendored_library = "libpolyvSDK/libpolyvSDK.a"
  s.vendored_frameworks = "libpolyvSDK/AdditionalLibrary/AlicloudHttpDNS.framework", "libpolyvSDK/AdditionalLibrary/AlicloudUtils.framework", "libpolyvSDK/AdditionalLibrary/UTDID.framework"

  s.frameworks = "SystemConfiguration", "MobileCoreServices", "CoreTelephony"
  s.libraries = "resolv", "z", "sqlite3.0", "icucore"

  s.requires_arc = true

  # s.xcconfig = {  'OTHER_LDFLAGS' => '-lPolyvSDK -lresolv -lz -lsqlite3.0 -licucore -framework MobileCoreServices -framework SystemConfiguration -framework CoreTelephony -framework AlicloudHttpDNS -framework AlicloudUtils -framework UTDID', 'FRAMEWORK_SEARCH_PATHS' => '"${PODS_ROOT}/libpolyvSDKK/libpolyvSDK/AdditionalLibrary/"', 'LIBRARY_SEARCH_PATHS' => '"${PODS_ROOT}/libpolyvSDKK/libpolyvSDK/"'} 

end
