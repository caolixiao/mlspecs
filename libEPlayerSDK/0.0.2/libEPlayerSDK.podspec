#
#  Be sure to run `pod spec lint libEPlayerSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "libEPlayerSDK"
  s.version      = "0.0.2"
  s.summary      = "光慧科技 E课堂 ～> 2.3.0"

  s.description  = "光慧科技 E课堂"

  s.homepage     = "http://nshare.yunsp.com.cn/eplayer/SDK/index.html"
 
  s.license      = "MIT"
 
  s.author             = { "caolixiao" => "caolixiao@yeah.net" }

  s.platform     = :ios, "8.0"

  s.ios.deployment_target = "8.0"

  s.source       = { :git => "git@bitbucket.org:caolixiao/libeplayersdk.git", :tag => "#{s.version}" }

  s.resources = "eplayer.bundle"

  # s.vendored_frameworks = "EplayerPluginFramework.framework"
  s.preserve_path = "EplayerPluginFramework.framework"
  s.weak_framework = "EplayerPluginFramework"
  s.frameworks = "MediaPlayer", "MobileCoreServices", "SystemConfiguration", "CoreMedia", "AudioToolbox", "AVFoundation", "CoreGraphics", "UIKit", "Foundation"

  s.libraries = "sqlite3.0", "xml2", "z", "c++abi", "c++"

  s.requires_arc = true

  # valid_archs = ['arm64', 'armv7',]
  # s.xcconfig = { 'VALID_ARCHS' =>  valid_archs.join(' '), 'FRAMEWORK_SEARCH_PATHS' => '"${PODS_ROOT}/libEPlayerSDK/"'} 
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '"${PODS_ROOT}/libEPlayerSDK/"'} 

end
