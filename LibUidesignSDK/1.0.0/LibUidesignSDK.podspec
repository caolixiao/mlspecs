Pod::Spec.new do |s|
    
    s.name         = "LibUidesignSDK"
    s.version      = "1.0.0"
    s.summary      = "LibUidesignSDK SDK."
    s.description  = "LibUidesignSDK SDK for iOS."
    
    s.homepage     = "http://www.toby.ml"
    s.license      = "MIT"
    s.author       = { "caolixiao" => "caolixiao@yeah.net" }
    
    s.platform     = :ios, "9"
    s.ios.deployment_target = "9"
    
    s.source       = {
        :git => "https://caolixiao@bitbucket.org/caolixiao/libuidesignsdk.git",
        :tag => "v#{s.version}"
    }
    
    s.requires_arc = true
    s.static_framework = true
    s.swift_version = '5.0'
    s.prefix_header_contents = <<-DESC
    
    DESC
    
    s.pod_target_xcconfig = {
        "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) LibUidesignSDK_NAME=#{s.name} LibUidesignSDK_VERSION=#{s.version}"
    }
    
    s.default_subspecs = ["Base", "CustomUI"]
    
    s.subspec "Base" do |ss|
        ss.source_files = ["Foundation/**/*.{h,m}", "UIKit/**/*.{h,m}", "Base/*.{h,m,swift}"]
        ss.public_header_files = [ "Foundation/**/*.h", "UIKit/**/*.h", "Base/*.h"]
        ss.frameworks = ["Foundation", "UIKit"]

        ss.resource_bundles = {
          'Uidesign' => ["Resource/Assets.xcassets"]
        }
        
        ss.dependency 'LibBaseSDK'
        ss.dependency 'SnapKit'
    end

    ### CustomUI
    s.subspec "CustomUI" do |ss|
        ss.public_header_files = "CustomUI/**/*.h"
        ss.source_files = "CustomUI/**/*.{h,m,mm,swift}"
        
        ss.dependency 'MJRefresh'
    end
    
    ### ThirdSDK
    s.subspec "ThirdSDK" do |ss|
      ### SDPhotoBrowser
      ss.subspec "SDPhotoBrowser" do |sss|
        sss.public_header_files =
        "ThirdSDK/SDPhotoBrowser/*.h"
        
        sss.source_files  =
        "ThirdSDK/SDPhotoBrowser/*.{h,m}"
      end
      
      ### MLLeftAlignedLayout
      ss.subspec "MLLeftAlignedLayout" do |sss|
        sss.public_header_files =
        "ThirdSDK/MLLeftAlignedLayout/*.h"
        
        sss.source_files  =
        "ThirdSDK/MLLeftAlignedLayout/*.{h,m}"
      end
      
      ### MLSpectrumView
      ss.subspec "MLSpectrumView" do |sss|
        sss.public_header_files =
        "ThirdSDK/MLSpectrumView/*.h"
        
        sss.source_files  =
        "ThirdSDK/MLSpectrumView/*.{h,m}"
      end
      
      ### MLImagePicker
      ss.subspec "ImagePicker" do |sss|
        sss.public_header_files =
        "ThirdSDK/MLImagePickerController/*.h"
        
        sss.source_files  =
        "ThirdSDK/MLImagePickerController/*.{h,m}"
      end
      
      ### MLWeb
      ss.subspec "MLWeb" do |sss|
        sss.public_header_files =
        "ThirdSDK/MLWebViewController/*.h",
        "ThirdSDK/MLWebViewController/**/*.h"
        
        sss.source_files  =
        "ThirdSDK/MLWebViewController/*.{h,m}",
        "ThirdSDK/MLWebViewController/**/*.{h,m}"
        
        ss.prefix_header_contents = <<-DESC
        #import "MLGlobalConfig.h"
        DESC
      end
    end

end
